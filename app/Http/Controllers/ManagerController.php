<?php

namespace App\Http\Controllers;
use Illuminate\Http\Request;
use App\Models\User;
use App\Models\JsAccounts;
use App\Models\LocalAccounts;
use Illuminate\Support\Facades\Redis;

class ManagerController extends Controller
{
    //
    private $client;

    public function __construct(){
        $this->client = new \GuzzleHttp\Client();
    }
    public function test(){
//        Redis::set('test', 'working');
        echo Redis::exists('test2');
    }
    public function login()  {
        $title = "Login";
        return view("login")->with('title', $title);

    }

    public function testAccount(){
        $user = new User();
        $user->name = 'sys_user';
        $user->email = 'test@gmail.com';
        $user->password = \Hash::make('test@gmail.com');
        $user->save();
    }
    public function refreshToken(Request $request){
        $id = $request->token;
        $js = JsAccounts::find($id);
        $token = $this->getFreshToken($js->email, $js->password, $js->file);
        $encoded = base64_encode($token);
        $f = fopen(storage_path('app/'.$js->file), 'w+');
        fwrite($f, $encoded);
        fclose($f);
        echo (json_decode($token)->status) ? "Token refreshed" : "Something is wrong check the password maybe";

    }

    private function getFreshToken($email, $password, $file) {
        $auth_url = 'https://ext.junglescout.com/api/v1/users/initial_authentication';
        $data = array(
            'username' => $email,
            'password' => $password,
            'app' => 'jsp'
        );

        $auth_data = $this->sendPostRequest($auth_url, $data, $this->getHeader());
        return $auth_data;
    }
    private function getHeader(){
        $header = array(
            'Host' => 'ext.junglescout.com',
            'Accept' => 'application/json, text/javascript, */*; q=0.01',
            'User-Agent' => 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/88.0.4324.150 Safari/537.36 Edg/88.0.705.63',
            'Content-Type' => 'application/x-www-form-urlencoded',
            'Origin' => 'chrome-extension://hbepkiiehjkmcoghfeffaepdhddbkkol',
            'Sec-Fetch-Dest' => 'empty',
            'Accept-Encoding' => 'gzip, deflate',
            'Accept-Language' => 'en-US,en;q=0.9',
            'Sec-Fetch-Site' => 'none',
            'Sec-Fetch-Mode' => 'cors',
            'Cookie' => 'AWSALB=RLvBRQVsqJrG/VWlIjJLqICypvkbG6306zqIGAtUniBcFCgOw2B6LweD5J7jiCkL9vacB2DZHSjFITSS6FpCSJVSq9L8Q0Qz+ESRZjYQX8oN3pLprgqUJAr2ZIRO; AWSALBCORS=RLvBRQVsqJrG/VWlIjJLqICypvkbG6306zqIGAtUniBcFCgOw2B6LweD5J7jiCkL9vacB2DZHSjFITSS6FpCSJVSq9L8Q0Qz+ESRZjYQX8oN3pLprgqUJAr2ZIRO',
        );
        return $header;
    }
    private function sendGetRequest($url, $header) {
        $res = $this->client->request('GET', $url, [
            'headers' => $header
        ]);
        return $res->getBody();
    }

    public function deleteLocalAccount($local_id) {
        $js = LocalAccounts::find($local_id)->delete();
        return redirect()->route('panel.scout.account.local');

    }
    public function deleteAccount($token){
        
        // echo "We are okay";
        $lcs = LocalAccounts::where('js_id', $token)->delete();
        $js = JsAccounts::find($token)->delete();
        // $js->
        return redirect()->route('panel.scout.account');

    }
    private function sendPostRequest($url, $data, $header){

        $res = $this->client->request('POST', $url, [
            'form_params' => $data,
            'headers' => $header
        ]);
        return $res->getBody();
    }

    public function dologin(Request $request) {
        if($this->validate($request, [
            'email' => 'required|email',
            'password' => 'required',
            ])) {
                if (\Auth::attempt([
                    'email' => $request->email,
                    'password' => $request->password])
                ){
                    return redirect()->route('panel.dashboard');
                }
                return redirect()->route('panel.login')->with('error', 'Invalid email address or password');
            } else {
                return redirect()->route('panel.login')->with('error', 'Invalid email');
            }

    }

    public function dashboard(){
        return view("dashboard");
    }


    public function createAccount(Request $request) {
        $this->validate($request, [
            'email' => 'required|email',
            'password' => 'required',
            ]);
        $user = new User();
        $user->name = 'sys_user';
        $user->email = $request->email;
        $user->password = Hash::make($request->password);
        $user->save();
    }
    private function generateRandomString($length = 10) {
        $characters = 'abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ';
        $charactersLength = strlen($characters);
        $randomString = '';
        for ($i = 0; $i < $length; $i++) {
            $randomString .= $characters[rand(0, $charactersLength - 1)];
        }
        return $randomString;
    }

    public function addScout(Request $request) {
        $file_name = $this->generateRandomString() .".txt";
        $js = new JsAccounts();
        $js->email = $request->email;
        $js->password = $request->password;
        $js->file = $file_name;
        $js->status = true;
        $js->save();
        return redirect()->route('panel.scout.account');
    }
    public function scout(Request $request){
        //displays js account already placed
        $jsaccounts = JsAccounts::all();
         return view('scout-account', compact('jsaccounts'));

    }
    public function localClown() {
        $jsaccounts = JsAccounts::all();
        $localaccount = LocalAccounts::all();
        return view('localaccount', compact('jsaccounts', 'localaccount'));
    }
    public function addLocalClown(Request $request) {
        $localaccount = new LocalAccounts();
        $localaccount->js_id = $request->js_id;
        $localaccount->email = $request->email;
        $localaccount->password = $request->password;
        $localaccount->save();
        return redirect()->route('panel.scout.account.local');
    }
    public function logout(Request $request)
    {
        if(\Auth::check())
        {
            \Auth::logout();
            $request->session()->invalidate();
        }
        return  redirect('panel/login');
    }
}
