<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use App\Models\JsAccounts;

class LocalAccounts extends Model
{
    use HasFactory;

    protected $fillable = [
        'js_id',
        'email',
        'password',
    ];
    protected $table = "localaccounts";

    public function js(){
        return $this->belongsTo(JsAccounts::class, 'js_id');
    }
}
