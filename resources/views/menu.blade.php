<li class="nav-item {{ (route('panel.dashboard') === url()->full()) ? 'active' : ''}}">
    <a class="nav-link" href="{{route('panel.dashboard')}}">
        <i class="material-icons">dashboard</i>
        <p>Dashboard</p>
    </a>
</li>
<li class="nav-item {{ (route('panel.scout.account') === url()->full()) ? 'active' : ''}} "><a class="nav-link" href="{{route('panel.scout.account')}}"><i class="material-icons">dashboard</i><p>Jungle Scout Account</p></a></li>
<li class="nav-item {{ (route('panel.scout.account.local') === url()->full()) ? 'active' : ''}} "><a class="nav-link" href="{{route('panel.scout.account.local')}}"><i class="material-icons">dashboard</i><p>Add Local Accounts</p></a></li>