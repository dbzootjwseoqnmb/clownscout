@extends('dashboard')
@section('title', 'JS Accounts')
@section('cardtitle', 'JS Accounts')
@section('cardsubtitle', 'The accounts which are provided by JungleScout ')
@section('body')

<div class="row">
<div class="col-md-8">
        <div class="card">
        <div class="card-header card-header-primary">
            <h4 class="card-title ">@yield('cardtitle')</h4>
            <p class="card-category">@yield('cardsubtitle')</p>
        </div>
        <div class="card-body">

        <section class="row">
            <div class="col-md-12">
                <table class="table">
                @php ($i = 1)
                <thead>
                    <th>#</th>
                    <th>Email</th>
                    <th>File</th>
                    <th>Status</th>
                    <th>Action</th>
                </thead>
                    @foreach($jsaccounts as $js)
                    <tr>
                        <td>{{$i++}}</td>
                        <td>{{$js->email}}</td>
                        <td>{{$js->file}}</td>
                        <td>{{($js->status) ? "active" : "inactive"}}</td>
                        <td class="td-actions text-right">
                            <button type="button" rel="tooltip" title="Refresh Token" class="btn btn-primary btn-link btn-sm">
                                <a target="_blank" href="{{route('panel.refresh.token')}}/{{$js->id}}">
                                    <i class="material-icons">refresh</i>
                                </a>
                            </button>
                            
                            <button type="button" rel="tooltip" title="Remove" class="btn btn-danger btn-link btn-sm">
                            <a class="tokendelete" href="{{route('panel.delete.token')}}/{{$js->id}}">
                                <i class="material-icons">close</i>
                            </a>
                            </button>
                        </td>
                    
                    </tr>
                    @endforeach

                </table>

            </div>

        </section>
        </div>
        </div>
    </div>
    
    
<div class="col-md-4">
    <div class="card">
        <div class="card-header card-header-primary">
            <h4 class="card-title ">Add JS Account</h4>
            <p class="card-category">Insert Valid Credentails</p>
        </div>
        <div class="card-body">
            <form action="{{route('panel.scout.add.account')}}" method="POST" >
                    <label class="bmd-label-floating">JS Account Email</label>
                    <input type="email" name="email" class="form-control">

                    <label class="bmd-label-floating">Password</label>
                    <input type="password" name="password" class="form-control">
                    {{csrf_field()}}
                    <input type="submit" class="btn btn-primary" value="Add JS Account">
            </form>
        </div>  
    </div>

    <div class="clearfix"></div>

</div>


</div>


<script>

$('.tokendelete').on('click', function(e){
    var c = confirm("If you delete this account, all LOCAL ACCOUNT associated with this account will be deleted. Are you sure you want to continue?");
    if(!c) {
        e.preventDefault();
    }
    // alert('ok');
});

</script>
@endsection
