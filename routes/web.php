<?php

use Illuminate\Support\Facades\Route;
use App\Http\Controllers\ScoutController;
/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome')->name('login');
});
Route::get('/test', 'ManagerController@test');

Route::group(['prefix'=>'panel','as'=>'panel.'], function(){
    Route::get('login', "ManagerController@login");
    Route::post('login', "ManagerController@dologin");
    // Route::get('test', "ManagerController@testAccount");

});

Route::group(['prefix' => 'panel', 'as' => 'panel.', 'middleware' => 'auth'], function(){
    Route::get('dashboard',"ManagerController@dashboard")->name('dashboard');
    Route::get('scout/account', "ManagerController@scout")->name('scout.account');
    Route::post('scout/add/account', "ManagerController@addScout")->name('scout.add.account');
    Route::get('scout/account/local', "ManagerController@localClown")->name('scout.account.local');
    Route::post('scout/account/local', "ManagerController@addLocalClown")->name('scout.add.account.local');
    Route::get('scout/refresh/token/', "ManagerController@refreshToken")->name('refresh.token');
    Route::get('scout/refresh/token/{token}', "ManagerController@refreshToken")->name('refresh.token.update');
    Route::get('scout/delete/token', 'ManagerController@deleteAccount')->name('delete.token');
    Route::get('scout/delete/token/{token}', 'ManagerController@deleteAccount')->name('delete.token.action');
    Route::get('scout/delete/local/', 'ManagerController@deleteLocalAccount')->name('delete.local');
    Route::get('scout/delete/local/{local_id}', 'ManagerController@deleteLocalAccount')->name('delete.local.action');
});

